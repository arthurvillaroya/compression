#include <stdio.h>
#include "image_ppm.h"
#include <iostream>

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250], cNomImgEcriteR[250], cNomImgEcriteG[250], cNomImgEcriteB[250];
  int nH, nW, nTaille;
  
  if (argc != 6) {
    printf("Usage: ImageIn.ppm ImageOut.ppm CompoR.pgm CompoG.pgm CompoB.pgm\n"); 
    exit (1) ;
  }
   
  sscanf (argv[1],"%s",cNomImgLue) ;
  sscanf (argv[2],"%s",cNomImgEcrite);
  
  sscanf (argv[3],"%s",cNomImgEcriteR) ;
  sscanf (argv[4],"%s",cNomImgEcriteG);
  sscanf (argv[5],"%s",cNomImgEcriteB);

  OCTET *ImgIn, *ImgOut, *ImgR, *ImgG, *ImgB, *ImgRCompresse, *ImgBCompresse, *ImgRDC, *ImgBDC;
   
  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  
  int nTaille3 = nTaille * 3;
  allocation_tableau(ImgIn, OCTET, nTaille3);
  lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
  allocation_tableau(ImgOut, OCTET, nTaille3);

  allocation_tableau(ImgR, OCTET, nTaille);
  allocation_tableau(ImgG, OCTET, nTaille);
  allocation_tableau(ImgB, OCTET, nTaille);
  allocation_tableau(ImgRDC, OCTET, nTaille);
  allocation_tableau(ImgBDC, OCTET, nTaille);

  allocation_tableau(ImgRCompresse, OCTET, nTaille/4);
  allocation_tableau(ImgBCompresse, OCTET, nTaille/4);

  for(int i = 0; i < nTaille3; i += 3){
    ImgR[i/3] = ImgIn[i];
    ImgG[i/3] = ImgIn[i+1];
    ImgB[i/3] = ImgIn[i+2];
  }

  double a = ceil(nH/2);
  double b = ceil(nW/2);

  int nH2 = a;
  int nW2 = b;

  for (int i = 0; i < nH2; i++){
    for (int j = 0; j < nW2; j++){
        ImgRCompresse[i * nW2 + j] = (ImgR[i*2 * nW + j*2] + ImgR[(i*2+1) * nW + j*2] + ImgR[i*2 * nW + (j*2+1)] + ImgR[(i*2+1) * nW + (j*2+1)])/4;
        ImgBCompresse[i * nW2 + j] = (ImgB[i*2 * nW + j*2] + ImgB[(i*2+1) * nW + j*2] + ImgB[i*2 * nW + (j*2+1)] + ImgB[(i*2+1) * nW + (j*2+1)])/4;
    }
  }

  for (int i = 0; i < nH; i++){
    for (int j = 0; j < nW; j++){
      ImgRDC[i * nW + j] = (ImgRCompresse[i/2 * nW2 + j/2] + ImgRCompresse[(i/2+1) * nW2 + j/2] + ImgRCompresse[i/2 * nW2 + (j/2+1)] + ImgRCompresse[(i/2+1) * nW2 + (j/2+1)])/4;
      ImgBDC[i * nW + j] = (ImgBCompresse[i/2 * nW2 + j/2] + ImgBCompresse[(i/2+1) * nW2 + j/2] + ImgBCompresse[i/2 * nW2 + (j/2+1)] + ImgBCompresse[(i/2+1) * nW2 + (j/2+1)])/4;
    }
  }

  for (int i = 0; i < nH2; i++){
    for (int j = 0; j < nW2; j++){
        ImgRDC[i*2 * nW + j*2] = ImgRCompresse[i * nW2 + j];
        ImgBDC[i*2 * nW + j*2] = ImgBCompresse[i * nW2 + j];
    }
  }

  for (int i = 0; i < nH; i++){
    for (int j = 1; j < nW; j+=2){
      ImgRDC[i * nW + j] = (ImgRDC[i * nW + j-1] + ImgRDC[i * nW + j+1])/2; 
      ImgBDC[i * nW + j] = (ImgBDC[i * nW + j-1] + ImgBDC[i * nW + j+1])/2;
    }
  }

  for (int i = 1; i < nH; i+=2){
    for (int j = 0; j < nW; j++){
      ImgRDC[i * nW + j] = (ImgRDC[(i-1) * nW + j] + ImgRDC[(i+1) * nW + j])/2; 
      ImgBDC[i * nW + j] = (ImgBDC[(i-1) * nW + j] + ImgBDC[(i+1) * nW + j])/2;
    }
  }


  for(int i = 0; i < nTaille3; i+=3){
    ImgOut[i] = ImgRDC[i/3];
    ImgOut[i+1] = ImgG[i/3];
    ImgOut[i+2] = ImgBDC[i/3];
  }


  ecrire_image_pgm(cNomImgEcriteR, ImgRCompresse,  nH/2, nW/2);
  ecrire_image_pgm(cNomImgEcriteB, ImgBCompresse,  nH/2, nW/2);
  ecrire_image_pgm(cNomImgEcriteG, ImgG,  nH, nW);
  ecrire_image_ppm(cNomImgEcrite, ImgOut, nH, nW);

  free(ImgIn);
  return 1;
}
