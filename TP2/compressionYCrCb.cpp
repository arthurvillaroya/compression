#include <stdio.h>
#include "image_ppm.h"
#include <iostream>
#include <algorithm>

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250], cNomImgEcriteY[250], cNomImgEcriteCr[250], cNomImgEcriteCb[250];
  int nH, nW, nTaille, nR, nB, nG;
  
  if (argc != 6) {
    printf("Usage: ImageIn.ppm ImageOut.ppm CompoY.pgm CompoCr.pgm CompoCb.pgm\n"); 
    exit (1) ;
  }
   
  sscanf (argv[1],"%s",cNomImgLue) ;
  sscanf (argv[2],"%s",cNomImgEcrite);
  
  sscanf (argv[3],"%s",cNomImgEcriteY) ;
  sscanf (argv[4],"%s",cNomImgEcriteCr);
  sscanf (argv[5],"%s",cNomImgEcriteCb);

  OCTET *ImgIn, *ImgOut, *ImgY, *ImgCr, *ImgCb, *ImgCrCompresse, *ImgCbCompresse, *ImgCrDC, *ImgCbDC;
   
  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  
  int nTaille3 = nTaille * 3;
  allocation_tableau(ImgIn, OCTET, nTaille3);
  lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
  allocation_tableau(ImgOut, OCTET, nTaille3);

  allocation_tableau(ImgY, OCTET, nTaille);
  allocation_tableau(ImgCr, OCTET, nTaille);
  allocation_tableau(ImgCb, OCTET, nTaille);
  allocation_tableau(ImgCrDC, OCTET, nTaille);
  allocation_tableau(ImgCbDC, OCTET, nTaille);

  allocation_tableau(ImgCrCompresse, OCTET, nTaille/4);
  allocation_tableau(ImgCbCompresse, OCTET, nTaille/4);

  for(int i = 0; i < nTaille3; i += 3){
        nR = ImgIn[i];
        nG = ImgIn[i+1];
        nB = ImgIn[i+2];
        ImgY[i / 3] = 0.299*nR + 0.587*nG + 0.114*nB;
        ImgCb[i / 3] = -0.1687 * nR - 0.3313 * nG + 0.5 * nB + 128;
        ImgCr[i / 3] = 0.5 * nR - 0.4187 * nG - 0.0813 * nB + 128;
  }

  double a = ceil(nH/2);
  double b = ceil(nW/2);

  int nH2 = a;
  int nW2 = b;

  for (int i = 0; i < nH2; i++){
    for (int j = 0; j < nW2; j++){
        ImgCrCompresse[i * nW2 + j] = (ImgCr[i*2 * nW + j*2] + ImgCr[(i*2+1) * nW + j*2] + ImgCr[i*2 * nW + (j*2+1)] + ImgCr[(i*2+1) * nW + (j*2+1)])/4;
        ImgCbCompresse[i * nW2 + j] = (ImgCb[i*2 * nW + j*2] + ImgCb[(i*2+1) * nW + j*2] + ImgCb[i*2 * nW + (j*2+1)] + ImgCb[(i*2+1) * nW + (j*2+1)])/4;
    }
  }

  for (int i = 0; i < nH; i++){
    for (int j = 0; j < nW; j++){
      ImgCrDC[i * nW + j] = (ImgCrCompresse[i/2 * nW2 + j/2] + ImgCrCompresse[(i/2+1) * nW2 + j/2] + ImgCrCompresse[i/2 * nW2 + (j/2+1)] + ImgCrCompresse[(i/2+1) * nW2 + (j/2+1)])/4;
      ImgCbDC[i * nW + j] = (ImgCbCompresse[i/2 * nW2 + j/2] + ImgCbCompresse[(i/2+1) * nW2 + j/2] + ImgCbCompresse[i/2 * nW2 + (j/2+1)] + ImgCbCompresse[(i/2+1) * nW2 + (j/2+1)])/4;
    }
  }

  for (int i = 0; i < nH2; i++){
    for (int j = 0; j < nW2; j++){
        ImgCrDC[i*2 * nW + j*2] = ImgCrCompresse[i * nW2 + j];
        ImgCbDC[i*2 * nW + j*2] = ImgCbCompresse[i * nW2 + j];
    }
  }

  for (int i = 0; i < nH; i++){
    for (int j = 1; j < nW; j+=2){
      ImgCrDC[i * nW + j] = (ImgCrDC[i * nW + j-1] + ImgCrDC[i * nW + j+1])/2; 
      ImgCbDC[i * nW + j] = (ImgCbDC[i * nW + j-1] + ImgCbDC[i * nW + j+1])/2;
    }
  }

  for (int i = 1; i < nH; i+=2){
    for (int j = 0; j < nW; j++){
      ImgCrDC[i * nW + j] = (ImgCrDC[(i-1) * nW + j] + ImgCrDC[(i+1) * nW + j])/2; 
      ImgCbDC[i * nW + j] = (ImgCbDC[(i-1) * nW + j] + ImgCbDC[(i+1) * nW + j])/2;
    }
  }


  for(int i = 0; i < nTaille3-nW*3; i+=3){
       nR = ImgY[i/3] + 1.402*(ImgCrDC[i/3] - 128);
       nG = ImgY[i/3] - 0.34414*(ImgCbDC[i/3] - 128) - 0.71414 * (ImgCrDC[i/3] - 128);
       nB = ImgY[i/3] + 1.772 * (ImgCbDC[i/3] - 128);
       ImgOut[i] = std::min(std::max(0,nR),255);
       ImgOut[i+1] = std::min(std::max(0,nG),255);
       ImgOut[i+2] = std::min(std::max(0,nB),255);
  }


  ecrire_image_pgm(cNomImgEcriteCr, ImgCrCompresse,  nH/2, nW/2);
  ecrire_image_pgm(cNomImgEcriteCb, ImgCbCompresse,  nH/2, nW/2);
  ecrire_image_pgm(cNomImgEcriteY, ImgY,  nH, nW);
  ecrire_image_ppm(cNomImgEcrite, ImgOut, nH, nW);

  free(ImgIn);
  return 1;
}
