#include <stdio.h>
#include "image_ppm.h"
#include <math.h>
#include <stdlib.h>
#include <iostream>

void ondelettes_haar(OCTET *ImgIn, OCTET *ImgBF, OCTET *ImgHF, OCTET *ImgHV, OCTET *ImgTHF, int nH, int nW){
  unsigned char A, B, C, D;
  unsigned char BF, HF, HV, THF;

  for(int i = 0; i < nH; i+=2){
    for(int j = 0; j < nW; j+=2){
      A = ImgIn[i*nW + j];
      B = ImgIn[i*nW + (j+1)];
      C = ImgIn[(i+1)*nW + j];
      D = ImgIn[(i+1)*nW + (j+1)];

      BF = (A+B+C+D)/4;
      HV = (A+B-C-D)/2 + 128; // MFh
      HF = (A-B+C-D)/2 + 128; // MFv
      THF = A-B-C+D + 128; // HF

      ImgBF[i/2*(nW/2) + j/2] = BF;
      ImgHF[i/2*(nW/2) + j/2] = HF;
      ImgHV[i/2*(nW/2) + j/2] = HV;
      ImgTHF[i/2*(nW/2) + j/2] = THF;
    }
  }
}

void ondelettes_haar_recursion(OCTET *ImgOut, OCTET *ImgBF,  OCTET *ImgHF,  OCTET *ImgHV,  OCTET *ImgTHF, int nH, int nW, int N){
  OCTET *initial_ImgBF = ImgBF;

    for(int i = 1; i <= N; i++){
        ondelettes_haar(ImgBF, ImgBF, ImgHF, ImgHV, ImgTHF, nH, nW);
        
        int hf = 0;
        for(int x = nH/(i*2); x < nH/i; x++){
            for(int y = 0; y < nW/(i*2); y++){
                ImgOut[x*nW + y] = ImgHF[hf]; // or HV ?
                hf++;
            }
        }

        int thf = 0;
        for(int x = nH/(i*2); x < nH/i; x++){
            for(int y = nW/(i*2); y < nW/i; y++){
                ImgOut[x*nW + y] = ImgTHF[thf]; 
                thf++;
            }
        }

        int hv = 0;
        for(int x = 0; x < nH/(i*2); x++){
            for(int y = nW/(i*2); y < nW/i; y++){
                ImgOut[x*nW + y] = ImgHV[hv]; 
                hv++;
            }
        }

        if(i==N){
        int bf = 0;
            for(int x = 0; x < nH/(i*2); x++){
                for(int y = 0; y < nW/(i*2); y++){
                    ImgOut[x*nW + y] = initial_ImgBF[bf]; 
                    bf++;
                }
            }
        }
    }
}

int main(int argc, char* argv[]){
    char cNomImgLue1[250], cNomImgLue2[250], cNomImgLue3[250];
    int nH, nW, nTaille, Recur;
    
    if (argc != 5) {
        printf("Usage: ImageIn1.pgm ImageOut.pgm ImageOut2.pgm Recur\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue1);
    sscanf (argv[2],"%s",cNomImgLue2);
    sscanf (argv[3],"%s",cNomImgLue3);
    sscanf (argv[4],"%d",&Recur);
    

    OCTET *ImgIn1, *ImgOut, *ImgOut2, *BF, *MFh, *MFv, *HF;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue1, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn1, OCTET, nTaille);
    lire_image_pgm(cNomImgLue1, ImgIn1, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);
    allocation_tableau(ImgOut2, OCTET, nTaille);

    allocation_tableau(BF, OCTET, nTaille/4);
    allocation_tableau(MFh, OCTET, nTaille/4);
    allocation_tableau(MFv, OCTET, nTaille/4);
    allocation_tableau(HF, OCTET, nTaille/4);

    ondelettes_haar(ImgIn1, BF, MFh, MFv, HF, nH, nW);
    ondelettes_haar_recursion(ImgOut, BF, MFh, MFv, HF, nH, nW, Recur);

    ecrire_image_pgm(cNomImgLue2, ImgOut,  nH, nW);
    ecrire_image_pgm(cNomImgLue3, ImgOut2,  nH, nW);
    return 0;
}