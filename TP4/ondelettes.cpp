#include <stdio.h>
#include "image_ppm.h"
#include <math.h>
#include <stdlib.h>
#include <iostream>

int main(int argc, char* argv[]){
    char cNomImgLue1[250], cNomImgLue2[250], cNomImgLue3[250];
    int nH, nW, nTaille;
    
    if (argc != 4) {
        printf("Usage: ImageIn1.pgm ImageOut.pgm ImageOut2.pgm \n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue1);
    sscanf (argv[2],"%s",cNomImgLue2);
    sscanf (argv[3],"%s",cNomImgLue3);
    

    OCTET *ImgIn1, *ImgOut, *ImgOut2, *BF, *MFh, *MFv, *HF;
   
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue1, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn1, OCTET, nTaille);
    lire_image_pgm(cNomImgLue1, ImgIn1, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);
    allocation_tableau(ImgOut2, OCTET, nTaille);

    allocation_tableau(BF, OCTET, nTaille/4);
    allocation_tableau(MFh, OCTET, nTaille/4);
    allocation_tableau(MFv, OCTET, nTaille/4);
    allocation_tableau(HF, OCTET, nTaille/4);

    int A;
    int B;
    int C;
    int D;

    for(int i = 0; i < nH; i+=2){
        
        for(int j = 0; j < nW; j+=2){
                            
            A = ImgIn1[i * nW + j];
            B = ImgIn1[i * nW + (j + 1)];
            C = ImgIn1[(i + 1) * nW + j];
            D = ImgIn1[(i + 1) * nW + (j + 1)];

            BF[(i/2 * nW/2 + j/2)] = (A + B + C + D) / 4;
            MFh[(i/2 * nW/2 + j/2)] = (A + B - C - D) / 2 + 128; 
            MFv[(i/2 * nW/2 + j/2)] = (A - B + C - D) / 2 + 128; 
            HF[(i/2 * nW/2 + j/2)] = (A - B - C + D) + 128;    
        }
    }

    /*LES QUATRES IMAGES*/
    for(int i = 0; i < nH/2; i++){
        for(int j = 0; j < nW/2; j++){
            ImgOut[i * nW + j] = BF[i * nW/2 + j];
        }
        for(int j = nW/2; j < nW; j++){
            ImgOut[i * nW + j] = MFv[i * nW/2 + (j-nW/2)];
        }
    }

    for(int i = nH/2; i < nH; i++){
        for(int j = 0; j < nW/2; j++){
            ImgOut[i * nW + j] = MFh[(i-nH/2) * nW/2 + j];
        }
        for(int j = nW/2; j < nW; j++){
            ImgOut[i * nW + j] = HF[(i-nH/2) * nW/2 + (j-nW/2)];
        }
    }

    /*LA RECONSTRUCTION*/
    for(int i = 0; i < nH; i+=2){
        for(int j = 0; j < nW; j+=2){
            A = std::min(255, std::max(0, BF[i/2 * (nW/2) + j/2] + (MFh[i/2 * (nW/2) + j/2] - 128)/2 + (MFv[i/2 * (nW/2) + j/2] - 128)/2 + (HF[i/2 * (nW/2) + j/2] - 128)/4));
            B = std::min(255, std::max(0, BF[i/2 * (nW/2) + j/2] + (MFh[i/2 * (nW/2) + j/2] - 128)/2 - (MFv[i/2 * (nW/2) + j/2] - 128)/2 - (HF[i/2 * (nW/2) + j/2] - 128)/4));
            C = std::min(255, std::max(0, BF[i/2 * (nW/2) + j/2] - (MFh[i/2 * (nW/2) + j/2] - 128)/2 + (MFv[i/2 * (nW/2) + j/2] - 128)/2 - (HF[i/2 * (nW/2) + j/2] - 128)/4));
            D = std::min(255, std::max(0, BF[i/2 * (nW/2) + j/2] - (MFh[i/2 * (nW/2) + j/2] - 128)/2 - (MFv[i/2 * (nW/2) + j/2] - 128)/2 + (HF[i/2 * (nW/2) + j/2] - 128)/4));

            ImgOut2[i*nW+j] = A;
            ImgOut2[i*nW+(j+1)] = B;
            ImgOut2[(i+1)*nW+j] = C;
            ImgOut2[(i+1)*nW+(j+1)] = D;
        }
    }

    ecrire_image_pgm(cNomImgLue2, ImgOut,  nH, nW);
    ecrire_image_pgm(cNomImgLue3, ImgOut2,  nH, nW);
    return 0;
}