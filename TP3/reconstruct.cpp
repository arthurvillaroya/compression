#include <stdio.h>
#include "image_ppm.h"
#include <iostream>
#include <stdlib.h>



int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;
    
    if (argc != 3){
        printf("Usage: ImageIn.pgm ImageOut.pgm\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite) ;
    

    OCTET *ImgIn, *ImgOut, *Predi;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);
    allocation_tableau(Predi, OCTET, nTaille);

    int a, b, c;
    double predi;
    int erreur = 0;

    Predi[0] = ImgIn[0];   

    for(int i = 0; i < nH; i++){
        for(int j = 0; j < nW; j++){
            a = (j > 0)? Predi[i * nW + (j-1)] : Predi[i * nW + j];
            b = (i > 0 && j > 0)? Predi[(i-1) * nW + (j-1)] : Predi[i * nW + j];
            c = (i > 0)? Predi[(i-1) * nW + j] : Predi[i * nW + j];

            if(i == 0 && j == 1){
                std::cout<<"B : "<<b<<"  C : "<<c<<std::endl;
            }

            Predi[i * nW + j] = (3 * (a + c) - 2 * b) / 4;
        }	
    } 


    for(int i = 0; i < nH; i++){
        for(int j = 0; j < nW; j++){
            int difference = Predi[i * nW + j] - ImgIn[i * nW + j] + 128;
            if(difference < 0 || difference > 255){erreur++;}
            ImgOut[i * nW + j] = std::max(0, std::min(255, difference));
        }	
    } 



    std::cout<<"le nombre de débordement est de : " << erreur << std::endl;
    ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);

    free(ImgIn); free(ImgOut);
    return 1;
}

