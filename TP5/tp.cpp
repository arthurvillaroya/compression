#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstdio>
#include <cstdlib>

#include <algorithm>
#include <GL/glut.h>
#include <float.h>
#include "Vec3.h"


struct Triangle {
    inline Triangle () {
        v[0] = v[1] = v[2] = 0;
    }
    inline Triangle (const Triangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
    }
    inline Triangle (unsigned int v0, unsigned int v1, unsigned int v2) {
        v[0] = v0;   v[1] = v1;   v[2] = v2;
    }
    unsigned int & operator [] (unsigned int iv) { return v[iv]; }
    unsigned int operator [] (unsigned int iv) const { return v[iv]; }
    inline virtual ~Triangle () {}
    inline Triangle & operator = (const Triangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
        return (*this);
    }
    // membres :
    unsigned int v[3];
};

void openOFF(std::string const & filename, std::vector<Triangle> & triangles, std::vector<Vec3> & vertices){
    std::ifstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open())
    {
        std::cout << filename << " cannot be opened" << std::endl;
        return;
    }

    std::string magic_s;

    myfile >> magic_s;

    if( magic_s != "OFF" )
    {
        std::cout << magic_s << " != OFF :   We handle ONLY *.off files." << std::endl;
        myfile.close();
        exit(1);
    }

    int n_vertices , n_faces , dummy_int;
    myfile >> n_vertices >> n_faces >> dummy_int;

    for( int v = 0 ; v < n_vertices ; ++v )
    {
        float x , y , z ;

        myfile >> x >> y >> z ;
        vertices.push_back( Vec3( x , y , z ) );
    }

    triangles.clear();
    for( int f = 0 ; f < n_faces ; ++f )
    {
        int n_vertices_on_face;
        myfile >> n_vertices_on_face;

        if( n_vertices_on_face == 3 )
        {
            unsigned int _v1 , _v2 , _v3;
            myfile >> _v1 >> _v2 >> _v3;

            triangles.push_back(Triangle( _v1, _v2, _v3 ));
        }
        else if( n_vertices_on_face == 4 )
        {
            unsigned int _v1 , _v2 , _v3 , _v4;
            myfile >> _v1 >> _v2 >> _v3 >> _v4;

            triangles.push_back(Triangle(_v1, _v2, _v3 ));
            triangles.push_back(Triangle(_v1, _v3, _v4));
        }
        else
        {
            std::cout << "We handle ONLY *.off files with 3 or 4 vertices per face" << std::endl;
            myfile.close();
            exit(1);
        }
    }
}

bool saveOFF(const std::string & filename, std::vector< Vec3 > & i_vertices, std::vector< Triangle > & i_triangles) {
    std::ofstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open()) {
        std::cout << filename << " cannot be opened" << std::endl;
        return false;
    }

    myfile << "OFF" << std::endl ;

    unsigned int n_vertices = i_vertices.size() , n_triangles = i_triangles.size();
    myfile << n_vertices << " " << n_triangles << " 0" << std::endl;

    for( unsigned int v = 0 ; v < n_vertices ; ++v ) {
        myfile << i_vertices[v][0] << " " << i_vertices[v][1] << " " << i_vertices[v][2] << " ";
        myfile << std::endl;
    }
    for( unsigned int f = 0 ; f < n_triangles ; ++f ) {
        myfile << 3 << " " << i_triangles[f][0] << " " << i_triangles[f][1] << " " << i_triangles[f][2];
        myfile << std::endl;
    }
    myfile.close();
    return true;
}

void pointMin(std::vector<Vec3> const & vertices, Vec3 & min, Vec3 & max){
    
	const double epsilon = 0.1;
	
    double Xmin = std::numeric_limits<double>::max();
	double Ymin = std::numeric_limits<double>::max();
	double Zmin = std::numeric_limits<double>::max();

	double Xmax = std::numeric_limits<double>::min();
	double Ymax = std::numeric_limits<double>::min();
	double Zmax = std::numeric_limits<double>::min();

	for(unsigned long int i = 0; i < vertices.size(); i++){
		if(vertices[i][0] < Xmin){Xmin=vertices[i][0];}
		if(vertices[i][1] < Ymin){Ymin=vertices[i][1];}
		if(vertices[i][2] < Zmin){Zmin=vertices[i][2];}

		if(vertices[i][0] > Xmax){Xmax=vertices[i][0];}
		if(vertices[i][1] > Ymax){Ymax=vertices[i][1];}
		if(vertices[i][2] > Zmax){Zmax=vertices[i][2];}
	}
    
    min = Vec3(Xmin, Ymin, Zmin);
    max = Vec3(Xmax, Ymax, Zmax);
}

double getRange(std::vector<Vec3> const & vertices, Vec3 const & min, Vec3 const & max){

    double Xrange = std::abs(max[0] - min[0]);
    double Yrange = std::abs(max[1] - min[1]);
    double Zrange = std::abs(max[2] - min[2]);

    return std::max(Xrange, std::max(Yrange, Zrange));
}

void quantification(std::vector<Vec3> & vertices, int qp, double range, Vec3 const & min){
    for(int i = 0; i < vertices.size(); i++){
        vertices[i][0] = floor((pow(2, qp) * (vertices[i][0] - min[0])) / range);
        vertices[i][1] = floor((pow(2, qp) * (vertices[i][1] - min[1])) / range);
        vertices[i][2] = floor((pow(2, qp) * (vertices[i][2] - min[2])) / range);
    }
}

void deQuantification(std::vector<Vec3> & vertices, int qp, double range, Vec3 const & min){
    for(int i = 0; i < vertices.size(); i++){
        vertices[i] = range * vertices[i] / pow(2, qp) + min;
    }
}

double distSquare(Vec3 const &V1, Vec3 const &V2){
	return (V2[0] - V1[0]) * (V2[0] - V1[0]) + (V2[1] - V1[1]) * (V2[1] - V1[1]) + (V2[2] - V1[2]) * (V2[2] - V1[2]);
}

double RMSE(std::vector<Vec3>  const & vertices, std::vector<Vec3> const & verticesQuant){

	double sum = 0;

	for (int i = 0; i < vertices.size(); ++i)
	{
		sum += distSquare(vertices[i], verticesQuant[i]);
	}

	sum /= vertices.size();

	return sqrt(sum);
}

int main (int argc, char ** argv) {

    int qp;

    sscanf (argv[1],"%d", &qp);

    std::vector<Vec3> verticesOut;
    std::vector<Triangle> trianglesOut;


    std::vector<Vec3> vertices;
    std::vector<Triangle> triangles;

    openOFF("bunny.off", triangles, vertices);
    openOFF("bunny.off", trianglesOut, verticesOut);

    Vec3 min;
    Vec3 max;

    pointMin(vertices, min, max);

    double range = getRange(vertices, min, max);
    
    quantification(verticesOut, qp, range, min);
    saveOFF("bunnyQuant.off", verticesOut, triangles);

    deQuantification(verticesOut, qp, range, min);

    std::cout<<RMSE(vertices,verticesOut)<<std::endl;

    saveOFF("bunny2.off", verticesOut, triangles);
}