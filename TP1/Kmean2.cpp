// test_couleur.cpp : Seuille une image couleur 

#include <stdio.h>
#include "image_ppm.h"
#include <iostream>
#include <vector>

bool couleurEgale(int* C1, int* C2){
  return C1[0] == C2[0] && C1[1] == C2[1] && C1[2] == C2[2];
}

bool quelleClasse(int* C1, int* C2, std::vector<int> pixel, int* classePlusProche){
  double distanceC1 = sqrt(pow((pixel[0] - C1[0]), 2) + pow((pixel[1] - C1[1]), 2) + pow((pixel[2] - C1[2]), 2));
  double distanceC2 = sqrt(pow((pixel[0] - C2[0]), 2) + pow((pixel[1] - C2[1]), 2) + pow((pixel[2] - C2[2]), 2));
  bool classe;

  if(distanceC1 < distanceC2){
    classePlusProche[0] = C1[0];
    classePlusProche[1] = C1[1];
    classePlusProche[2] = C1[2];
    classe = true;
  }
  else{
    classePlusProche[0] = C2[0];
    classePlusProche[1] = C2[1];
    classePlusProche[2] = C2[2];
    classe = false;
  }

  return classe;
}

void moyClasse(std::vector<std::vector<int>> &classe, int* somme){
  unsigned long int taille = classe.size();

  somme[0] = 0; somme[1] = 0; somme[2] = 0;

  for(int i = 0; i < taille; i++){
    somme[0] += classe[i][0];
    somme[1] += classe[i][1];
    somme[2] += classe[i][2];
  }

  if(taille > 0){
    somme[0] = somme[0]/taille;
    somme[1] = somme[1]/taille;
    somme[2] = somme[2]/taille;
  }
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite1[250], cNomImgEcrite2[250];
  int nH, nW, nTaille, nR, nG, nB, x1, y1, x2, y2;
  
  if (argc != 8) {
    printf("Usage: ImageIn.ppm ImageOut1.ppm ImageOut2.ppm coordX1 coordY1 coordX2 coordY2 \n"); 
    exit (1) ;
  }
   
  sscanf (argv[1],"%s",cNomImgLue) ;
  sscanf (argv[2],"%s",cNomImgEcrite1);
  sscanf (argv[3],"%s",cNomImgEcrite2);
  sscanf (argv[4],"%d",&x1);
  sscanf (argv[5],"%d",&y1);
  sscanf (argv[6],"%d",&x2);
  sscanf (argv[7],"%d",&y2);

  OCTET *ImgIn, *ImgOut1, *ImgOut2;
   
  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;

  int nTaille3 = nTaille * 3;
  allocation_tableau(ImgIn, OCTET, nTaille3);
  lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
  allocation_tableau(ImgOut1, OCTET, nTaille3);
  allocation_tableau(ImgOut2, OCTET, nTaille3);

  std::cout<<nH<<" | "<<nW<<std::endl;

  int P1[3];
  int P2[3];

  int moyP1[3]{0};
  int moyP2[3]{0};
  int moyP1Temp[3]{0};
  int moyP2Temp[3]{0};

  int index1 = (y1 * nW + x1) * 3;
  int index2 = (y2 * nW + x2) * 3;

  P1[0] = ImgIn[index1]; P1[1] = ImgIn[index1+1]; P1[2] = ImgIn[index1 + 2];
  P2[0] = ImgIn[index2]; P2[1] = ImgIn[index2+1]; P2[2] = ImgIn[index2 + 2];

  std::cout<<"classe A : "<<P1[0]<<" | "<<P1[1]<<" | "<<P1[2]<<std::endl;
  std::cout<<"classe B : "<<P2[0]<<" | "<<P2[1]<<" | "<<P2[2]<<std::endl;

  std::vector<std::vector<int>> classe1;
  std::vector<std::vector<int>> classe2;

  for (int i = 0; i < nTaille3; i += 3){
    std::vector<int> pixel;
    pixel.push_back(ImgIn[i]);
    pixel.push_back(ImgIn[i+1]);
    pixel.push_back(ImgIn[i+2]);

    int vraiCouleur[3]; 
    if(quelleClasse(P1, P2, pixel, vraiCouleur)){
      classe1.push_back(pixel);
    }
    else{
      classe2.push_back(pixel);
    }

    ImgOut1[i] = vraiCouleur[0];
    ImgOut1[i+1] = vraiCouleur[1];
    ImgOut1[i+2] = vraiCouleur[2];
  }
  
  moyClasse(classe1, moyP1Temp);
  moyClasse(classe2, moyP2Temp);

  int ite = 0;
  std::cout<<"avant convergeance A : "<<moyP1Temp[0]<<" | "<<moyP1Temp[1]<<" | "<<moyP1Temp[2]<<std::endl;
  std::cout<<"avant convergeance B : "<<moyP2Temp[0]<<" | "<<moyP2Temp[1]<<" | "<<moyP2Temp[2]<<std::endl;
  
  while(!couleurEgale(moyP1Temp, moyP1) || !couleurEgale(moyP2Temp, moyP2)){
    ite++;
    moyP1[0] = moyP1Temp[0]; moyP1[1] = moyP1Temp[1]; moyP1[2] = moyP1Temp[2];
    moyP2[0] = moyP2Temp[0]; moyP2[1] = moyP2Temp[1]; moyP2[2] = moyP2Temp[2];
    
    classe1.clear();
    classe2.clear();

    for (int i = 0; i < nTaille3; i += 3){
      std::vector<int> pixel;
      pixel.push_back(ImgIn[i]);
      pixel.push_back(ImgIn[i+1]);
      pixel.push_back(ImgIn[i+2]);

      int vraiCouleur[3]; 
      if(quelleClasse(moyP1Temp, moyP2Temp, pixel, vraiCouleur)){
        classe1.push_back(pixel);
      }
      else{
        classe2.push_back(pixel);
      }
    }

    moyClasse(classe1, moyP1Temp);
    moyClasse(classe2, moyP2Temp);
  }

  std::cout<<"après convergeance A : "<<moyP1Temp[0]<<" | "<<moyP1Temp[1]<<" | "<<moyP1Temp[2]<<std::endl;
  std::cout<<"après convergeance B : "<<moyP2Temp[0]<<" | "<<moyP2Temp[1]<<" | "<<moyP2Temp[2]<<std::endl;
  std::cout<<"nombre d'itération de convergeance : "<<ite<<std::endl;

  for(int i = 0; i < nTaille3; i += 3){
    std::vector<int> pixel;
    pixel.push_back(ImgIn[i]);
    pixel.push_back(ImgIn[i+1]);
    pixel.push_back(ImgIn[i+2]);

    int vraiCouleur[3]; 
    
    bool a = quelleClasse(moyP1Temp, moyP2Temp, pixel, vraiCouleur);

    ImgOut2[i] = vraiCouleur[0];
    ImgOut2[i+1] = vraiCouleur[1];
    ImgOut2[i+2] = vraiCouleur[2];
  }
  
  ecrire_image_ppm(cNomImgEcrite1, ImgOut1,  nH, nW);
  ecrire_image_ppm(cNomImgEcrite2, ImgOut2,  nH, nW);
  free(ImgIn);
  return 1;
}
