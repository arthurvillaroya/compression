// test_couleur.cpp : Seuille une image couleur 

#include <stdio.h>
#include "image_ppm.h"
#include <iostream>
#include <vector>
#include <limits>
#include <stdlib.h>     

int distCouleur(int C1, int C2){
  return abs(C1 - C2);
}

bool couleurEgale(std::vector<int> &C1, std::vector<int> &C2){
  return (distCouleur(C1[0], C2[0]) == 0) && (distCouleur(C1[1], C2[1]) == 0) && (distCouleur(C1[2], C2[2]) == 0);
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.ppm ImageOut.ppm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, 6);
   
   
   ImgOut[0] = ImgIn[0];
   ImgOut[1] = ImgIn[1];
   ImgOut[2] = ImgIn[2];
   
   std::vector<int> C1; C1.push_back(ImgIn[0]); C1.push_back(ImgIn[1]); C1.push_back(ImgIn[2]);
   std::vector<int> C2; 
   
   for (int i=0; i < nTaille3; i+=3){
       C2.clear();
       C2.push_back(ImgIn[i]); C2.push_back(ImgIn[i+1]); C2.push_back(ImgIn[i+2]);
       
      if(!(couleurEgale(C1, C2))){
        ImgOut[3] = ImgIn[i];
   	    ImgOut[4] = ImgIn[i+1];
   	    ImgOut[5] = ImgIn[i+2];
   	    break;
      }
    }

   ecrire_image_ppm(cNomImgEcrite, ImgOut,  1, 2);
   free(ImgIn);
   return 1;
}
