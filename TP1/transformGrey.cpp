// test_couleur.cpp : Seuille une image couleur 

#include <stdio.h>
#include "image_ppm.h"
#include <iostream>
#include <vector>
#include <limits>
#include <stdlib.h>     
#include <time.h>

bool appartientTab(int val, std::vector<int> tab){
    bool estDanstableau = false;
    for(int i = 0; i < tab.size(); i++){
        if(tab[i] == val){
            estDanstableau = true;
        }
    }
    if(!estDanstableau){tab.push_back(val);}
    return estDanstableau;
}

void mettreAEgalite(std::vector<std::vector<int>> &C1, std::vector<std::vector<int>> &C2){
    unsigned long int taille = C1.size();

    for(unsigned long int i = 0; i < taille; i++){
        C1[i][0] = C2[i][0];
        C1[i][1] = C2[i][1];
        C1[i][2] = C2[i][2];
    }
}

int distCouleur(int C1, int C2){
    return abs(C1 - C2);
}

bool distMin(std::vector<int> &C1, std::vector<int> &C2){
    return (distCouleur(C1[0], C2[0]) <= 2) && (distCouleur(C1[1], C2[1]) <= 2) && (distCouleur(C1[2], C2[2]) <= 2);
}

bool couleurEgale(std::vector<int> &C1, std::vector<int> &C2){
    return C1[0] == C2[0] && C1[1] == C2[1] && C1[2] == C2[2];
}

bool moyennesEgale(std::vector<std::vector<int>> &C1, std::vector<std::vector<int>> &C2){
    unsigned long int taille = C1.size();

    for(unsigned long int i = 0; i < taille; i++){
        if(!distMin(C1[i], C2[i])){return false;}
    }

    return true;
}

int quelleClasse(std::vector<std::vector<int>> &classes, std::vector<int> &pixel, int* classePlusProche){
    double distanceMax = std::numeric_limits<double>::max(); 
    unsigned long int taille = classes.size();
    double newDist;
    int index;

    for(unsigned long int i = 0; i < taille; i++){
        newDist = sqrt(pow((pixel[0] - classes[i][0]), 2) + pow((pixel[1] - classes[i][1]), 2) + pow((pixel[2] - classes[i][2]), 2));
        if(newDist < distanceMax){
            distanceMax = newDist;
            index = i;
        }
    }

    classePlusProche[0] = classes[index][0];
    classePlusProche[1] = classes[index][1];
    classePlusProche[2] = classes[index][2];

    return index; 
}

void moyClasse(std::vector<std::vector<std::vector<int>>> &classes, std::vector<std::vector<int>> &somme, std::vector<std::vector<int>> &couleurClasse){
    unsigned long int taille = classes.size();
    std::vector<int> remplissage; remplissage.push_back(0); remplissage.push_back(0); remplissage.push_back(0);
    
    somme.clear();
    for(unsigned long int i = 0; i < taille; i++){
        somme.push_back(remplissage);
    }

    for(unsigned long int i = 0; i < taille; i++){
        unsigned long int taille2 = classes[i].size();

        for(unsigned long int j = 0; j < taille2; j++){
            somme[i][0] += classes[i][j][0];
            somme[i][1] += classes[i][j][1];
            somme[i][2] += classes[i][j][2];
        }

        if(taille2 > 0){
            somme[i][0] /= taille2;
            somme[i][1] /= taille2;
            somme[i][2] /= taille2;
        }
        else{
            somme[i][0] = couleurClasse[i][0];
            somme[i][1] = couleurClasse[i][1];
            somme[i][2] = couleurClasse[i][2]; 
        }
    }
}

int IndexDansPalette(std::vector<std::vector<int>> &palette, std::vector<int> &pixel){
    for(int i = 0; i < palette.size(); i++){
        if(couleurEgale(palette[i], pixel)){
            return i;
        }
    }
}

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite1[250], cNomImgEcrite2[250], cNomImgEcrite3[250];
    int nH, nW, nTaille;
    
    if (argc != 5) {
        printf("Usage: ImageIn.ppm ImageOutClasse.ppm ImageOutGrey.pgm palette.ppm\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite1);
    sscanf (argv[3],"%s",cNomImgEcrite2);
    sscanf (argv[4],"%s",cNomImgEcrite3);

    OCTET *ImgIn, *ImgOutGrey, *ImgOut2, *palette;
    
    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    int nTaille3 = nTaille * 3;
    allocation_tableau(ImgIn, OCTET, nTaille3);
    lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOutGrey, OCTET, nTaille);
    allocation_tableau(ImgOut2, OCTET, nTaille3);


    std::cout<<nH<<" | "<<nW<<std::endl;
    
    std::vector<std::vector<int>> couleursClasses;
    std::vector<std::vector<std::vector<int>>> moyennesClasses;
    std::vector<std::vector<int>> moyennes;
    std::vector<std::vector<int>> moyennesTemp;

    for(int i = 0; i < 256; i++){
        std::vector<std::vector<int>> remplissage;
        moyennesClasses.push_back(remplissage);
        

        std::vector<int> tableauIndex;
        int index;
        do{
            int x = rand() % (nW+1);
            int y = rand() % (nH+1);

            index = y * nW + x;
        }while(appartientTab(index, tableauIndex));

        std::vector<int> couleur;
        couleur.push_back(ImgIn[index]); couleur.push_back(ImgIn[index + 1]); couleur.push_back(ImgIn[index + 2]);

        couleursClasses.push_back(couleur);
    }

    for(int i = 0; i < nTaille3; i += 3){
        std::vector<int> pixel;
        pixel.push_back(ImgIn[i]);
        pixel.push_back(ImgIn[i+1]);
        pixel.push_back(ImgIn[i+2]);

        int vraiCouleur[3];
        int index = quelleClasse(couleursClasses, pixel, vraiCouleur);
        
        moyennesClasses[index].push_back(pixel);
    }

    
    std::vector<int> remplissage; remplissage.push_back(0); remplissage.push_back(0); remplissage.push_back(0);
    for(unsigned long int i = 0; i < moyennesClasses.size(); i++){
        moyennes.push_back(remplissage);
        moyennesTemp.push_back(remplissage);
    }
    
    moyClasse(moyennesClasses, moyennes, couleursClasses);

    int ite = 0;
    while(!moyennesEgale(moyennes, moyennesTemp)){
        ite++;
        std::cout<<"itération : "<<ite<<std::endl;
        mettreAEgalite(moyennesTemp, moyennes);

        unsigned long int taille = moyennesClasses.size();
        for(unsigned long int i = 0; i < taille; i++){
            moyennesClasses[i].clear();
        }

        for(int i = 0; i < nTaille3; i += 3){
            std::vector<int> pixel;
            pixel.push_back(ImgIn[i]);
            pixel.push_back(ImgIn[i+1]);
            pixel.push_back(ImgIn[i+2]);

            int vraiCouleur[3];
            int index = quelleClasse(moyennes, pixel, vraiCouleur);
            
            moyennesClasses[index].push_back(pixel);
        }

        moyClasse(moyennesClasses, moyennes, couleursClasses);
    }
    std::cout<<"itération : FINI"<<std::endl;

    for(int i = 0; i < nTaille3; i += 3){
        std::vector<int> pixel;
        pixel.push_back(ImgIn[i]);
        pixel.push_back(ImgIn[i+1]);
        pixel.push_back(ImgIn[i+2]);

        int vraiCouleur[3]; 
        
        int a = quelleClasse(moyennes, pixel, vraiCouleur);

        ImgOut2[i] = vraiCouleur[0];
        ImgOut2[i+1] = vraiCouleur[1];
        ImgOut2[i+2] = vraiCouleur[2];
    }

    for(int i = 0; i < nTaille3; i += 3){
        std::vector<int> pixel;
        pixel.push_back(ImgOut2[i]);
        pixel.push_back(ImgOut2[i+1]);
        pixel.push_back(ImgOut2[i+2]);

        int indexGrey = IndexDansPalette(moyennes, pixel);

        ImgOutGrey[i/3] = indexGrey;
    }

    allocation_tableau(palette, OCTET, moyennes.size()*3);
    
    for(int i = 0; i < moyennes.size()*3; i += 3){
        palette[i] = moyennes[i/3][0];
        palette[i+1] = moyennes[i/3][1];
        palette[i+2] = moyennes[i/3][2];
    }
    

    ecrire_image_ppm(cNomImgEcrite1, ImgOut2,  nH, nW);
    ecrire_image_pgm(cNomImgEcrite2, ImgOutGrey,  nH, nW);
    ecrire_image_ppm(cNomImgEcrite3, palette, 16, 16); 
    free(ImgIn);
    return 1;
}
