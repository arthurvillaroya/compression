#include <stdio.h>
#include "image_ppm.h"
#include <math.h>
#include <stdlib.h>
#include <iostream>

#define VALMAX 255

double EQM(OCTET* ImgIn1, OCTET* ImgIn2, int nTaille){
    
    int nTaille3 = nTaille * 3;

    double eqmR = 0;
    double eqmG = 0;
    double eqmB = 0;

    for(int i = 0; i < nTaille3; i += 3){
        eqmR += pow((ImgIn1[i] - ImgIn2[i]), 2);
        eqmG += pow((ImgIn1[i + 1] - ImgIn2[i + 1]), 2);
        eqmB += pow((ImgIn1[i + 2] - ImgIn2[i + 2]), 2); 
    }

    eqmR /= nTaille;
    eqmG /= nTaille;
    eqmB /= nTaille;

    std::cout<<eqmR<<std::endl;
    std::cout<<eqmG<<std::endl;
    std::cout<<eqmB<<std::endl;

    std::cout<<(eqmR + eqmG + eqmB)/3<<std::endl;

    return (eqmR + eqmG + eqmB)/3;
}

int main(int argc, char* argv[]){
    char cNomImgLue1[250], cNomImgLue2[250];
    int nH, nW, nTaille;
    
    if (argc != 3) {
        printf("Usage: ImageIn1.ppm ImageIn2.ppm\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue1) ;
    sscanf (argv[2],"%s",cNomImgLue2);

    OCTET *ImgIn1, *ImgIn2;
   
    lire_nb_lignes_colonnes_image_ppm(cNomImgLue1, &nH, &nW);
    nTaille = nH * nW;

    int nTaille3 = nTaille * 3;
    allocation_tableau(ImgIn1, OCTET, nTaille3);
    lire_image_ppm(cNomImgLue1, ImgIn1, nH * nW);

    allocation_tableau(ImgIn2, OCTET, nTaille3);
    lire_image_ppm(cNomImgLue2, ImgIn2, nH * nW);

    double PSNR = 10 * log10((VALMAX*VALMAX)/EQM(ImgIn1, ImgIn2, nTaille));

    std::cout<<"PSNR : "<<PSNR<<std::endl;

    return 0;
}